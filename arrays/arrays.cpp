#include <iostream>
using namespace std;

int main()
{
	// We can easily create on variable
	float myTestScore = 98.9f;

	// We can create a list of variables using
	// the following notation;

	const int N_STUDENTS_IN_CLASS = 23;
	// Allocate N_STUDENTS_IN_CLASS floating point variables
	// I can reference these using indeces 0 through 22
	// 
	float studentScores[N_STUDENTS_IN_CLASS];

	// Set all the studentScores to 100.
	studentScores[0] = 100.0f;
	studentScores[1] = 100.0f;
	studentScores[2] = 100.0f;

	// For i = 0 to 23 set the ith studentScore to 100.0f
	for (int i = 0; i < N_STUDENTS_IN_CLASS; i++)
	{
		studentScores[i] = 100.0f - i;
	}


	// Find the sum of all the student scores.
	// When we haven't added any student scores the sum is 0.
	float sumOfStudentScores = 0;

	// For each student score
	for (int i = 0; i < N_STUDENTS_IN_CLASS; i++)
	{
		// Add the score to the sum
		// sumOfStudentScores += studentScores[i];
		sumOfStudentScores = sumOfStudentScores + studentScores[i];
	}

	float average = sumOfStudentScores / (float)N_STUDENTS_IN_CLASS;
	cout << "The class average is " << average << endl;

	// How many scores are less than 90?
	float CUT_OFF_SCORE = 90.0f;
	int numberOFScoresLessThanCutOff = 0;

	for (int i = 0; i < N_STUDENTS_IN_CLASS; i++)
	{
		if (studentScores[i] < CUT_OFF_SCORE)
		{
			// numberOFScoresLessThanCutOff += 1;
			// numberOFScoresLessThanCutOff = 
			// numberOfScoresLessThanCutOff + 1; 
			++numberOFScoresLessThanCutOff;
		}
	}

	cout << "Number of scores < " << CUT_OFF_SCORE << " is " <<
		numberOFScoresLessThanCutOff << endl;



	// List the student scores in reverse order.
	cout << "The student scores in reverse order are:" << endl;
	for (int i = N_STUDENTS_IN_CLASS - 1; i >= 0; i--)
	{
		cout << studentScores[i] << endl;
	}

	// Print every other student score
	//   (print a score then skip a score)
	for (int i = 0; i < N_STUDENTS_IN_CLASS; i = i + 2)
	{
		cout << studentScores[i] << endl;
	}

	// A less efficient solution is:
	for (int i = 0; i < N_STUDENTS_IN_CLASS; i++)
	{
		if (i % 2 == 0)
		{
			cout << studentScores[i] << endl;
		}
	}


	// Prompt the user for each of the values in the array
	for (int i = 0; i < N_STUDENTS_IN_CLASS; i++)
	{
		cout << "Enter the score for student #" << i + 1 << ": ";
		cin >> studentScores[i];
	}



	system("pause");
}