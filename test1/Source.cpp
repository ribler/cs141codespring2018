#include <iostream>
using namespace std;


void preprint(int n)
{
	if (n > 0)
	{
		preprint(n - 1);
		for (int i = 0; i < n; i++) cout << "*";
		cout << endl;
	}
}

void postprint(int n)
{
	if (n > 0)
	{
		for (int i = 0; i < n; i++) cout << "*";
		cout << endl;
		postprint(n - 1);
	}
}

void printAstrisks(int n)
{
	preprint(n);
	postprint(n);
}

int main()
{
	int month;
	int day;

	cout << "Enter a month in the range [1-12]:  ";
	cin >> month;
	cout << "Enter a day: ";
	cin >> day;

	const int JAN = 1;
	const int FEB = 2;
	const int MAR = 3;
	const int APR = 4;
	const int MAY = 5;
	const int AUG = 8;
	const int SEP = 9;
	const int NOV = 11;
	const int DEC = 12;

	// The fall semester at Lynchburg College begins on August 27 and continues 
	// until December 15.  The spring semester runs from January 14 to May 6.  
	// Spring break is March 3 � March 9.  Given two integers, month and day, 
	// write an if - statement that prints "in session" 
	// if the specified date occurs when the college is in session, and prints 
	// "not in session" otherwise.

	// Sufficient conditions to determine that we are in the fall semester
	//   It is August and the day is >= 27
	//   It is any day in September through November
	//   It is December and the day is <= 15

	// Sufficient conditions to determine that we are in the spring semester
	//   It is January and the day is >= 14
	//   It is February
	//   It is March and the day is < 3 or it is March and the day is > 9
	//   It is April
	//   It is May and the day is <= 6

	if ( (month == AUG && day >= 27) ||
		 (month >= SEP && month <= NOV) ||
		 (month == DEC && day <= 15) ||
		 (month == JAN &&  day >= 14) ||
		 (month == FEB) ||
		 (month == MAR && day < 3) ||
		 (month == MAR && day > 9) ||
		 (month == APR) ||
		 (month == MAY && day <= 6) )
	{
		cout << "in session." << endl;
	}
	else
	{
		cout << "not in session" << endl;
	}

	// Question #4
	int startNumber;
	int endNumber;
	functionToSetStartAndEnd(startNumber, endNumber);

	int sum = 0;
	for (int i = startNumber + 1; i < endNumber; i++)
	{
		// Check to see if I is odd
		if (i % 2 == 1)
		{
			sum += i;
		}
	}
	
	if (startNumber % 2 == 0)
	{ 
		startNumber = startNumber + 1;
	}
	else
	{
		startNumber = startNumber + 2;
	}

	sum = 0;
	for (int i = startNumber; i < endNumber; i = i + 2)
	{
		sum = sum + i;
	}

	// Question #5
	const int ARRAY_SIZE = 1000;
	int myArray[ARRAY_SIZE];
	for (int i = 0; i < ARRAY_SIZE; i++)
	{
		myArray[i] = 100 - i;
	}

	// Question #6
	const int N_ROWS = 12;
	const int N_COLUMNS = 12;
	int multTable[N_ROWS][N_COLUMNS];

	for (int row = 0; row < N_ROWS; ++row)
	{
		for (int column = 0; column < N_COLUMNS; ++column)
		{
			multTable[row][column] = (row + 1) * (column + 1);
			cout << multTable[row][column] << "\t"; // "\t" is tab
		}
		cout << endl;
	}





	// Question #7

		// According to a legend, a king was so pleased with the game of chess that 
		// he offered the inventor of the game the opportunity to name his own 
		// reward.The inventor asked the king to place a single grain of rice on the 
		// first square of the board, and then double the amount of rice on each 
		// subsequent square.If we fill the chessboard one row at a time, the 
		// first 9 values(indicating the number of grains of rice) would appear 
		// as shown below.
		//     1   2   4   8   16   32   64   128
		//     256

		//  First Bullet - 
		//
		// A normal chessboard has 8 rows and 8 columns, but I want you to write a 
		// program fragment for a chess board that has 6 rows and 5 columns.Your 
		// program should declare a two-dimensional int array, and then fill that 
		// array, as shown in the diagram, all the way up to and including the 
		// 30th square.
		// The first dimension is the number of rows, the second dimension is
		// the number of columns.

	    // Answer to first bullet:
		const int N_ROWS = 6;
		const int N_COLUMNS = 5;
		int riceChessBoard[N_ROWS][N_COLUMNS];

		int nGrains = 1;
		for(int row=0; row<N_ROWS; ++row)
		{
			for (int column = 0; column < N_COLUMNS; ++column)
			{
				riceChessBoard[row][column] = nGrains;
				nGrains = nGrains * 2;
			}
		}

		// Second Bullet
		// How many grains of rice will the inventor receive on the 30th square?

		// Answer:
		// The values are all powers of 2 starting with 2^0 for the first 
		// square.  The number of grains of rice on any square is 2^(n-1) where
		// n is the square number.  2^0 for the first square, 2^1 for the second
		// square and so on.  The 30th square will have 2^29 grains of rice.

		// Third Bullet
		// Why will your program fail if I ask you to rewirte the program for 
		// board dimensions 8x8
		// It will fail because 2^63 is much larger than 2^31-1 (the largest value
		// available to a signed integer.

		// Fourth Bullet
		// The program will fail on square 32 which has 2^31 grains on it.

		// Question 8
		// A Tribinocci series is a series in which the nth number in the series is 
		// the sum of the previous 3 numbers.We will start with the numbers 1, 1, 1, 
		// so the fourth number in the series will be 3 = (1 + 1 + 1), 
		// followed by 5 = (1 + 1 + 3), 9 = (1 + 3 + 5), 17 = (3 + 5 + 9) and so on.

		// Start of Tribinocci Series : 1, 1, 1, 3, 5, 9, 17, 31, 57, �

		// Write a loop to find the first number in a Tribinocci series that exceeds 
		// 100,000.

		int threeAgo = 1;
		int twoAgo = 1;
		int oneAgo = 1;
		int current = oneAgo + twoAgo + threeAgo;

		const int TARGET_TO_EXCEED = 100000;

		// While we haven't reached the target
		while(current <= TARGET_TO_EXCEED)
		{
			// Compute the next value in the series
			current = oneAgo + twoAgo + threeAgo;

			// move the element two back from the current position to three back
			threeAgo = twoAgo;

			// move the element one back from the current position to two back
			twoAgo = oneAgo;

			// move the element int the current position to one back
			oneAgo = current;
		}

		cout << current << " is the first number in the series to exceed " <<
			TARGET_TO_EXCEED << endl;



	system("pause");
}
