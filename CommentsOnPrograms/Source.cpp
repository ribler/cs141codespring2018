#include <iostream>
using namespace std;

int main()
{
	// 2 Reasons to use constants
	//   1) Make the code easier to understand
	//   2) If we need to change a value we change in 
	//      just one place.

	const int DAYS_PER_WEEK = 7;
	const int MAX_SPEED_IN_MPH = 100;
	int something = 234;
	int day = something % DAYS_PER_WEEK;

	// If the two reasons don't apply, don't use them.
	// Usual exceptions are 0, 1, and -1
	// Never include the value of the constant in the name
	const int SEVEN_DAY_WEEK = 7;
	const int ONE = 1;

	for (int day = 0; day < DAYS_PER_WEEK; day++)
	{
	}

	// Variable names
	//  -- Take some time to think about them.
	//  -- Try not to have similar names
	//       -- velocity1 and velocity2
	//       -- priceOfItem and itemPrice
	//       -- priceInDollars and priceInCents
	//       
	int asldkfjal;
	// if (asldkfjal)

	// Try not to add or subtract 1 or 2 in a lot of places
	int n = 10;
	for (int i = 0; i <= n; i++)
	{

	}





	system("pause");
}