#include <iostream>
using namespace std;

void paritionOnThree(double* theArray, int nElements)
{
	double* leftSwap = theArray;

	// theArray + nElements is one position past the end of 
	// theArray.  So computer theArray + nElements - 1;
	double* rightSwap = theArray + nElements - 1;

	while (leftSwap < rightSwap)
	{
		// Find an item on the left side that is on the wrong side
		while (*leftSwap < 3.0 && leftSwap < rightSwap)
			++leftSwap;

		// Find an item on the right side that is on the wrong side
		while (*rightSwap > 3.0 && leftSwap < rightSwap)
			++rightSwap;

		if (leftSwap < rightSwap)
		{
			// Exchange the values

			// Save the value at leftSwap before we clobber it.
			double temp = *leftSwap;

			// Copy *rightSwap to *leftSwap
			*leftSwap = *rightSwap;

			// Get the old value of *leftSwap from temp.
			*rightSwap = temp;
		}
	}



}

float*** allocate3DArray(int nRows, int nColumns, int nTables)
{
	// Allocate an array of pointers to 2-dimensional arrays
	float*** new3D = new float**[nTables];

	// For each pointer in new3d, allocate a table
	for (int i = 0; i < nTables; i++)
	{
		// new3D[i] = allocate2DArray(nRows, nColumns);

		// Allocate an array of pointer for the rows
		new3D[i] = new float*[nRows];
		for (int j = 0; j < nRows; j++)
		{
			new3D[i][j] = new float[nColumns];
		}
	}

	return  new3D;
}


// Write a function to dynamically allocate a 
// two-dimensional array of floats.
float** allocate2DArray(int nRows, int nColumns)
{
	// Allocate an array of pointers, one for each row
	float** newArray = new float*[nRows];

	// For each row, allocate the memory for those rows
	for (int i = 0; i < nRows; i++)
	{
		// Allocate the memory for that row.
		newArray[i] = new float[nColumns];
	}

	return newArray;
}

void delete2DArray(float** theArray, int nRows)
{
	// Delete the memory for each row
	for(int i=0;i<nRows;i++)
	{ 
		delete[] theArray[i];
	}

	// Delete the array of pointers to rows
	delete[] theArray;
}
    

int stringLength(const char* theString)
{
	int length = 0;
	while (*theString != 0)
	{
		++length;
		++theString;
	}
	return length;
}

// Write a function dynamically allocate memory for a and copy
// a null-terminated character string
char* allocateAndCopy(const char* thingToCopy)
{
	// Find the length of the thing to copy
	int length = stringLength(thingToCopy);

	// Dynamically Allocate memory to hold the copy (length of thingToCopy + 1)
	char* copy = new char[length + 1];

	// copy the values from thingToCopy to the newly allocated memory.
	//for (int i = 0; i <= length; i++)
	//{
	//	copy[i] = thingToCopy[i];
	//}

	char* startOfCopy = copy;
	while (*thingToCopy != 0)
	{
		// Copy a character to copy
		*copy = *thingToCopy;
		++copy;
		++thingToCopy;
	}
	// Null terminate the copy
	*copy = 0;

	return startOfCopy;
}

int main()
{

	int nRows = 23;
	int nColumns = 6;

	// Create a multiplication table with a jagged 2-d array
	// Allocate an array of pointers, one for row
	float** multTable = new float*[nRows];

	// Start with a row size of 1
	// Increment that by one for each new row
	int nextRowSize = 1;

	// For each row
	for (int row = 0; row < nRows; ++row)
	{
		multTable[row] = new float[nextRowSize];
		for (int column = 0; column < nextRowSize; ++column)
		{
			multTable[row][column] = row * column;
		}
		++nextRowSize;
	}

	// Deallocate the multTable
	for (int row = 0; row < nRows; ++row)
	{
		delete[] multTable[row];
	}

	// Delete the array of row pointers
	delete[] multTable;




	float** myNewArray = allocate2DArray(nRows, nColumns);

	int nTables = 2;
	float*** myNew3D = allocate3DArray(nRows, nColumns, nTables);

	for (int table = 0; table < nTables; ++table)
	{
		for (int row = 0; row < nRows; ++row)
		{
			for (int column = 0; column < nColumns; ++column)
			{
				myNew3D[table][row][column] = 0;
			}
		}
	}

	// Set all the elements in myNewArray to zero

	// For every row
	for (int row = 0; row < nRows; ++row)
	{
		// For each element in that row
		for(int column=0; column<nColumns; ++column)
		{ 
			myNewArray[row][column] = 0;
		}
	}

	// Delete the dynamically allocated array.
	delete2DArray(myNewArray, nRows);


	char* copyOfHello = allocateAndCopy("Hi!");
	cout << copyOfHello << endl; // should print "Hi!"

	// Use array notation on the dynamically allocated memory
	cout << copyOfHello[0] << endl; //  print 'H'
	cout << *(copyOfHello + 0) << endl; // print 'H'
	cout << copyOfHello[2] << endl; // print '!'
	cout << *(copyOfHello + 2) << endl; // print '!'

	// Create an arary of pointers
	const int N_POINTERS = 4;
	int* myPointers[N_POINTERS];
	int** addressOfMyPointers = myPointers;

	// Set each pointer to some dynamically allocated memory
	const int MEMORY_SIZE = 3;

	for (int i = 0; i < N_POINTERS; i++)
	{
		// Allocate memory for each pointer
		myPointers[i] = new int[MEMORY_SIZE];
	}

	myPointers[0][0] = 1;
	myPointers[2][1] = 2;

	const int N_ROWS = N_POINTERS;
	const int N_COLUMNS = MEMORY_SIZE;
	for (int row = 0; row < N_ROWS; ++row)
	{
		for (int column = 0; column < N_COLUMNS; ++column)
		{
			myPointers[row][column] = 0;
		}
	}

	// One way to create a 2-dimensional array
	//   1) Create an array of pointers (one for each row)
	//   2) Allocate memory for each of the those rows (size is the number of columns)

	// How can we dynamically allocate an array of pointers to ints?

	// <data type>* <variable> = new <data type>[<how many I want>];
	int* newArray = new int[10];
	nRows = 4;
	nColumns = N_COLUMNS;

	int** twoDArray = new int*[nRows];

	// Allocate the memory for each row
	for (int i = 0; i < nRows; i++)
	{
		twoDArray[i] = new int[nColumns];
	}

	// Now we can treat twoDArray like a 2-d array
	for (int row = 0; row < nRows; ++row)
	{
		for (int column = 0; column < nColumns; ++column)
		{
			twoDArray[row][column] = 0;
		}
	}

	// Delete each of the array rows individually
	for (int i = 0; i < nRows; i++)
	{
		delete[] twoDArray[i];
	}

	delete[] twoDArray;



	// We have to delete the memory that was allocated in allocateAndCopy
	delete[] copyOfHello;

	// Dynamically allocate an array of pointers to 2d arrays
	// int*** my3DArray = new int**[n2DArrays];


	// These are "automatic" variables and constants
	// Allocated automatically on the "runtime stack"
	// We must know MAX_SCORES at compile time.
	const int MAX_SCORES = 500;
	float studentScores[MAX_SCORES];


	int nStudents;
	cout << "Enter the number of students: ";
	cin >> nStudents;

	// Another option is to dynamically allocate the memory
	// With dynamic memory allocation we don't need to know the 
	// size until runtime.

	// Dynamic memory allocation occurs in response to the "new" operator
	// As "parameters" new needs to know the data type that we want and
	// the number of elements that we want.
	// new will return a pointer to the allocated memory

	// new dataType
	// what is returned is of dataType*

	// We are explicitly allocating the memory using new
	// Any time we new, we should ask ourselves this question:
	// "Where is this memory getting deallocated?"

	float* ptrToScores = new float[nStudents];

	for (int i = 0; i < nStudents; i++)
	{
		cout << "Enter the score for student " << i + 1 << " : ";
		cin >> ptrToScores[i];
	}
	
	// This program just prints the scores
	for (int i = 0; i < nStudents; i++)
	{
		cout << "The score for student " << i + 1 << " is: " <<
			ptrToScores[i] << endl;
	}

	//for (int i = 0; i < nStudents; i++)
	//{
	//	cout << "Enter the score for student " << i + 1 << " : ";
	//	cin >> *ptrToScores++;
	//}

	// Student scores will not be used any more
	// We can delete them here.

	// To delete an array we use the [] to indicate we are deleting
	// an array.
	delete [] ptrToScores;

	system("pause");
}