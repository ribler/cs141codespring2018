#include <iostream>
#include <string>
#include <vector>
using namespace std;

// An Abstract Data Type (ADT) is a data type that is defined
// strictly in terms of the operations that you can perform on it.

// Object-oriented programming allows us to create our own
// data types and implement them so that they serve as 
// Abstract Data Types

// Create a string from const null-terminated char string
// Delete its own memory
// Find the length
// Copy the string
// Append to strings
// Add character to the end
// Find the nth char in the string

void myFunction(const string& thingToPrint)
{
	cout << thingToPrint << endl;
}

int main()
{
	// ADT vector
	//   Create a vector
	//   Delete a vector
	//   Change the size 
	//   Find the length
	//   Read/Write the ith the value
	//   Copy to another vector
	//   Append to the end of the vector
	//   Prepend to the start of the vector

	vector<int> myVector;

	// Find the size of the vector
	int vectorSize = myVector.size();

	// Append to the end of the vector
	myVector.push_back(234);
	cout << myVector.size() << endl;

	myVector.push_back(1234);
	cout << myVector.size() << endl;

	for (int i = 0; i < myVector.size(); i++)
	{
		cout << myVector[i] << endl;
	}

	vector<int> anotherVector = myVector;


	string myString;

	cout << "Enter any string:";
	cin >> myString;
	cout << "Your string was: " << endl;

	string anotherString = "abcd";
	myString = anotherString;

	// Find the length of the string
	int length = anotherString.length();

	// Give myString a new value
	myString = "abcdefghijz";
	myString = anotherString + myString;
	myFunction(myString);
}






