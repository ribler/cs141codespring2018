#include <iostream>
using namespace std;

// A function to print the company logo
// This function does not compute and return a value so we refer to its return
// value as void.
// This function has no input parameters -- () is an empty list of input parameters

void printLogo()
{
	cout << "*************************" << endl;
	cout << "*************************" << endl;
	cout << "          ****           " << endl;
	cout << "          ****           " << endl;
	cout << "          ****           " << endl;
	cout << "          ****           " << endl;
	cout << "          ****           " << endl;
	cout << "          ****           " << endl;
	cout << "*************************" << endl;
	cout << "*************************" << endl;
	cout << endl;
}

int firstFib()
{
	return 0;
}

int daysPerNonLeapYearMonth(int month)
{
	const int JAN = 1;
	const int FEB = 2;
	const int MAR = 3;
	const int APR = 4;
	const int MAY = 5;
	const int JUN = 6;
	const int JUL = 7;
	const int AUG = 8;
	const int SEP = 9;
	const int OCT = 10;
	const int NOV = 11;
	const int DEC = 12;

	int nDays;
	switch (month)
	{
		case SEP:
		case APR:
		case JUN:
		case NOV:
			nDays = 30;
			break;

		case FEB:
			nDays = 28;
			break;

		case JAN:
		case MAR:
		case MAY:
		case JUL:
		case AUG:
		case OCT:
		case DEC:
			nDays = 31;
			break;
	}

	return nDays;
}

// Use pass by reference so that we can change the value of x
// Append an & to the data type to indicate pass by reference.
void times10(int& x)
{
	x = x * 10;
}

int getIntInRange(int minValue, int maxValue)
{
	int inputValue;
	do
	{
		cin >> inputValue;
		if (inputValue < minValue || inputValue > maxValue)
		{
			cout << inputValue << " is not in the range ["
				<< minValue << ", " << maxValue << "]" << endl;
		}
	} while (inputValue < minValue || inputValue > maxValue);

	minValue = -10;

	return inputValue;
}

// When this function invoked we execute the code inside the curly braces
// and return an integer as the value of the function.
int getValueInRange()
{
	const int MIN_MONTH = 1;
	const int MAX_MONTH = 12;
	int inputValue;

	do
	{
		cin >> inputValue;
		if (inputValue < MIN_MONTH || inputValue > MAX_MONTH)
		{
			cout << inputValue << " is not in the range ["
				<< MIN_MONTH << ", " << MAX_MONTH << "]" << endl;
		}
	} while (inputValue < MIN_MONTH || inputValue > MAX_MONTH);

	return inputValue;
}

// quotient and remainder are passed by reference
// Sometimes there are referred to as dummy parameters
// because we don't really use them as inputs, they
// are just used to provide outputs.
void intDivision(int numerator, int denominator,
	int& quotient, int& remainder)
{
	quotient = numerator / denominator;
	remainder = numerator % denominator;
}

// quotient and remainder are passed by reference
// Sometimes there are referred to as dummy parameters
// because we don't really use them as inputs, they
// are just used to provide outputs.
int anotherIntDivision(int numerator, int denominator,
	int& remainder)
{
	int quotient = numerator / denominator;
	remainder = numerator % denominator;
	return quotient;
}

char findCharWithSmallestValue(char first,
	char second, char third, char fourth,
	char fifth, char sixth)
{
	char smallest = first;

	// Some new C++ syntax - the question mark operator
	// (condition) ? <value if condition is true> :
	//               <value if condition is false>;

	//smallest = (smallest > second) ? second : smallest;
	//smallest = (smallest > third) ? third : smallest;
	//smallest = (smallest > fourth) ? fourth : smallest;
	//smallest = (smallest > fifth) ? fifth : smallest;
	//smallest = (smallest > sixth) ? sixth : smallest;

	if (smallest > second) smallest = second;
	if (smallest > third) smallest = third;
	if (smallest > fourth) smallest = fourth;
	if (smallest > fifth) smallest = fifth;
	if (smallest > sixth) smallest = sixth;

	return smallest;
}

int main()
{
	// Write a function that computes the integer division
	// of two numbers and the remainder from that division

	// Here is a new method to initialize an array in C++
	// The curly brackets contain the initial values
	// The number of elements in the array is determined
	// by the size of the list.
	const int N_CHARS_IN_MYCHARS = 6;
	char myChars[N_CHARS_IN_MYCHARS] 
		= { 'a', 'b', 'c', 'd', 'f', 'j'};


	char least = findCharWithSmallestValue(myChars[0],
		myChars[1], myChars[2], myChars[3],
		myChars[4], myChars[5]);

	cout << "least is " << least << endl;

	int q;
	int r;
	int dividend;
	int divisor;
	cout << "Enter dividend: ";
	cin >> dividend;
	cout << "Enter divisor: ";
	cin >> divisor;
	intDivision(dividend, divisor, q, r);
	cout << "The quotient is " << q << endl;
	cout << "The remainder is " << r << endl;

	q = anotherIntDivision(dividend, divisor, r);


	int myInt = 1;

	// I want myInt to change value to 10
	times10(myInt);

	const int MIN_MONTH = 1;
	const int MAX_MONTH = 12;

	for (int i = MIN_MONTH; i <= MAX_MONTH; i++)
	{
		int daysPerMonth = daysPerNonLeapYearMonth(i);
		cout << "Month " << i << " has " << daysPerMonth
			<< " days." << endl;
	}
	printLogo();


	int month;
	cout << "Enter the first month [1-12]: ";

	// Invoke the getValueInRange function.
	// Place the returned value in the variable month.
	month = getValueInRange();

	// Find the number of days in the month
	int daysPerMonth = daysPerNonLeapYearMonth(month);
	cout << "Enter a day [1-" << daysPerMonth << "]: ";
	int dayOfMonth = getIntInRange(1, daysPerMonth);

	// get a value between -234 and 235423
	int someNumber = getIntInRange(-234, 235423);
	int minValue = 23;
	int maxValue = 234;

	// Let's look at just the code from here
	int min = 0;
	int max = 10;
	int someFunctionValue = getIntInRange(min, max);
	// to here.

	for (int i = 0; i < 2; i++)
	{
		int anotherNumber = getIntInRange(minValue+i, maxValue-i);
	}




	int secondMonth;
	cout << "Enter the second month [1-12]: ";
	secondMonth = getValueInRange();

	printLogo();

	int day;
	cout << "Enter the day [1-31]: ";
	cin >> day;

	int x = firstFib();
	int y = firstFib();

	printLogo();
	printLogo();
	printLogo();
	printLogo();

	system("pause");
}