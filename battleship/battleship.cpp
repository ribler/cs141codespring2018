#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
	// Create a two-dimensional array for the
	// game board.
	const int MAX_BOARD_WIDTH = 26;
	const int MAX_BOARD_HEIGHT = 2048;
	char theBoard[MAX_BOARD_HEIGHT][MAX_BOARD_WIDTH];

	// Get the board size from the user
	//  - Make sure the numbers are within bounds
	int boardWidth;
	int boardHeight;

	do
	{
		cout << "Enter the board width [1-" <<
			MAX_BOARD_WIDTH << "]: ";
		cin >> boardWidth;

		if ((boardWidth > MAX_BOARD_WIDTH) ||
			(boardWidth <= 0))
		{
			cout << boardWidth << " is not in the range [1-"
				<< MAX_BOARD_WIDTH << ". " << endl;
		}
	} while ((boardWidth > MAX_BOARD_WIDTH) ||
		(boardWidth <= 0));

	do
	{
		cout << "Enter the board height [1-" <<
			MAX_BOARD_HEIGHT << "]: ";
		cin >> boardHeight;

		if ((boardHeight > MAX_BOARD_HEIGHT) ||
			(boardHeight <= 0))
		{
			cout << boardHeight << " is not in the range [1-"
				<< MAX_BOARD_HEIGHT << ". " << endl;
		}
	} while ((boardHeight > MAX_BOARD_HEIGHT) ||
		(boardHeight <= 0));

	// Set all the values in the usable board to
	// space

	// for every row
	//   for every column
	//       set the value to space
	for (int row = 0; row < boardHeight; ++row)
	{
		for (int column = 0; column < boardWidth; ++column)
		{
			 theBoard[row][column] = ' ';
		}
	}

	// Manually place a battle ship on the board
	int battleShipRow = (boardHeight-1) / 2 + 1;
	int battleShipColumn = (boardWidth-1) / 2 + 1;
	theBoard[battleShipRow][battleShipColumn] = 'B';

	// Print the board

	// set the margin size
	const int MARGIN_COLUMNS = 6;
	for (int i = 0; i < MARGIN_COLUMNS; ++i)
	{
		cout << ' ';
	}

	// Write the letters across the top
	// Start with A
	char letterToPrint = 'A';
	for (int i = 0; i < boardWidth; i++)
	{
		cout << letterToPrint << " ";
		++letterToPrint;
	}
	cout << endl;

	for (int row = 0; row < boardHeight; ++row)
	{
		// Print the row number
		cout << setw(MARGIN_COLUMNS-1) << row + 1 << " ";

		// For every column, print the value of the column
		for (int column = 0; column < boardWidth; ++column)
		{
			cout << theBoard[row][column] << " ";
		}

		// Print newline when you get to the end of the row.
		cout << endl;
	}

	system("pause");
}