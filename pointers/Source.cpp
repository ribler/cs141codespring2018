#include <iostream>
using namespace std;

// Use a function prototype
// Prototype is a bodyless function
// This declaration of the function, rather 
// than a definition of the function.
#include "stringfunctions.h"
// void myFunction();

void myOtherFunction()
{
	myFunction();
}

void myFunction()
{
	cout << "Hello" << endl;
}
// Write a function that takes a pointer to a null-terminated
// character string an replaces all 'a's with 'x'
// 
//   char myString[] = "aroma";
//   replaceAwithX(myString);
//   myString is now "xromx"

// Write a function that adds two int arrays
// a[] = { 1, 2, 3}
// b[] = { 10, 20, 30}
// c[] = a[] + b[] = {11, 22, 33}
void addArrays(const int* a, const int* b, int* c, int nElements)
{
	for (int i = 0; i < nElements; i++)
	{
		c[i] = a[i] + b[i];
	}
}



void printStringInReverseOrder(const char* theStringToPrint)
{
	// Start endPtr to point to the start of the string
	const char* endPtr = theStringToPrint;

	// Advance endPtr to the end of the array
	while (*endPtr != 0)
	{
		++endPtr;
	}

	// Move back before the null-terminator
	// This will make endPtr point to the last printable character
	--endPtr;

	while (endPtr >= theStringToPrint)
	{
		cout << *endPtr;
		--endPtr;
	}

}

void replaceAwithX(char* someString)
{
	// While we haven't encountered a null-terminator
	while(*someString != 0)
	{
		// Check to see if the letter is 'a'
		if (*someString == 'a')
		{
			// Change the value to 'x'
			*someString = 'x';
		}

		// move to the next letter in the string
		++someString;
	}
}

// Write a function to find the length of a string
//  (number of chars excluding the null-terminator

// When we pass pointers to functions, we want to use
// const pointers whenever we can.

// const char* ptr means we can't change the values that ptr points to.
// We can change ptr itself though.

// We can protect ptr itself by placing a const after the *
// char* const ptr;  // protect the pointer itself, not what it points to.
// const char* const ptr; // protects both the pointer and what it points to.


int stringLength(const char* ptr)
{
	int counter = 0;

	// While we are not pointing to a null terminator.
	while (*ptr != 0)
	{
		// Advance to the next char in the array
		++ptr;

		// Add one to the counter
		++counter;
	}
	return counter;
}


// Write a function that takes a pointer to an int
// array and changes all the values less than 0 to 0.
void negativesToZero(int* ptr, int nElements)
{
	// For every element in the array
	for (int i = 0; i < nElements; i++)
	{
		// if the element is less than zero
		if (*ptr < 0)
		{
			// Change it to zero
			*ptr = 0;
		}

		// Move to the next memory location
		++ptr;
	}
}

// Write a function that takes a pointer to an int
// array and changes all the values less than 0 to 0.
void negativesToZeroWithArrayNotation(int* ptr, 
	int nElements)
{
	// For every element in the array
	for (int i = 0; i < nElements; i++)
	{
		// if the element is less than zero
		if (ptr[i] < 0)
		{
			// Change it to zero
			ptr[i] = 0;
		}
	}
}



// Write a function that takes a pointer to an int as
// a parameter (use pass-by-value), and adds one to the value that 
// the pointer points to.  I can return void.

//  The parameter is a variable that holds the address of the
// value we want to change.
void pointerAdd(int* ptr)
{
	// Change the value at ptr to that value plus 1
	*ptr = *ptr + 1;
}

void printArray(const int* arrayPtr, int arraySize)
{
	for (int i = 0; i < arraySize; i++)
	{
		cout << *arrayPtr << endl;

		// Increment to the next address in the array
		arrayPtr = arrayPtr + 1;
	}
}

// Parameters
//   The location of float array that we will process
//   The size of the float array
//   The divisor to use on each item
void divideArrayBySomething(float* ptrToArray,
	int size, float divisor)
{
	for (int i = 0; i < size; i++)
	{
		// change the value of an array item by dividing
		// by divisor
		*ptrToArray = *ptrToArray / divisor;

        // ++ptrToArray;
		// int x = 1; //
		// cout << ++x << endl; // print 2
		// cout << x++ << endl;  // print 2
		// cout << x << endl; // print 3

		ptrToArray = ptrToArray + 1;

	}
}


int main()
{
	int x = 10;
	float y = 3.14f;

	const int ADD_ARRAY_SIZE = 3;
	int u[ADD_ARRAY_SIZE] = { 1, 2, 3 };
	const int v[ADD_ARRAY_SIZE] = { 10, 20, 30 };
	int w[ADD_ARRAY_SIZE];

	addArrays(u, v, w, ADD_ARRAY_SIZE);

	const char alpha[] = "abcd";
	int lengthOfAlpha = stringLength(alpha);

	char beta[] = "abcd";

	int lengthOfBeta = stringLength(beta);
	replaceAwithX(beta);

	// find the location (address) at which x is stored

	// We use the ampersand (&) to provide the operator
	// "the address of"
	// Read &x as "the address of x" << endl;

	// Get the address of x
	cout << hex << &x << endl;

	// The data type of &x is the address of an int
	// We declare variables that hold addresses using
	// the suffix *
	// &x - addressOf(x)
	int* addressOfX = &x;
	pointerAdd(addressOfX);
	pointerAdd(&x);

	float* addressOfY = &y;
	
	// Variables that store the addresses of other variables are
	// called "pointers."


	// If I have the address of a variable, I may be able to 
	// manipulate it.
	//   Read it.
	//   Write it.

	cout << addressOfX << endl;  // Write the address of x. (100)

	// The * operator should be read as (the value at)
	// Print the value at 100
	cout << *addressOfX << endl;  

	// The * operator can also be used to write a value
	// The value at 100 is assigned 11
	// The * operation performs the function we call "dereference"
	*addressOfX = 11;


	char myChar = 'r';

	// Store the address of myChar in a variable called 
	// myCharPtr.
	char* myCharPtr = &myChar;

	// Change the value of myChar to 'w'
	// without using myChar explicitly
	*myCharPtr = 'w';

	const int N_ELEMENTS_IN_ARRAY = 3;
	int myArray[N_ELEMENTS_IN_ARRAY]
		= { 1, 2, 3 };

	// An array name without subscripts evaluates to the
	// address of the first element in the array.
	int* ptrToMyArray = myArray;


	// We can also use a ptr as if it were an array name.
	// This will print myArray[0]
	//   ptrToMyArray and myArray are both the address of
	//   the first element in myArray.
	cout << ptrToMyArray[0] << endl;
	cout << *ptrToMyArray << endl;

	cout << ptrToMyArray[1] << endl;
	cout << *(ptrToMyArray + 1) << endl;

	printArray(myArray, N_ELEMENTS_IN_ARRAY);


	// Point to the next element in the array
	++ptrToMyArray;

	// We can't increment myArray because it is const
	// ++myArray;  // is illegal

	// How do we find the address of an array
	// Should we use the & operator?
	// We could do that, but there is a better method

	// An array name without subscripts evaluates to the
	// address of the first element in the array.
	// Print the address of myArray
	cout << hex << myArray << endl;

	// Store the address of myArray in 
	// addressOfMyArray
	int* addressOfMyArray = myArray;
	char greeting[] = "Hello";
	char greet[] = "Hi";

	cout << "The length of Hello is " << stringLength("Hello")
		<< endl;

	cout << "The length of Hello is "
		<< stringLength(greeting) << endl;

	cout << "The length of Hi is "
		<< stringLength(greet) << endl;


	const int SIZE_OF_MY_INT_ARRAY = 5;
	int myIntArray[] = { 23, -5, 235, -53, 1 };
	negativesToZero(myIntArray, SIZE_OF_MY_INT_ARRAY);


	// Store the address of x


	system("pause");
}