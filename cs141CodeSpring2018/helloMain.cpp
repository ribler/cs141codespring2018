#include <iostream>
using namespace std;

// Cs141 - Spring 2018
int main()
{
	cout << "Hello world!" << endl;
	const float PI = 3.14f;
	const int NEAR_PI = 3;
	int nearPi = (int) PI;

	// Create an integer variable called
	// dayOfTheMonth that consumes 4 bytes
	// The statement where we specify the
	// name and data type of the variable is 
	// called a "declaration."
	int dayOfTheMonth = 24;
	cout << "The value of dayOfTheMonth is " <<
		dayOfTheMonth << endl;

	// Change the value in dayOfTheMonth to 25
	dayOfTheMonth = 25;

	// Create another integer
	// This is "uninitialized"
	int dayInTheFuture;

	// Set the value of dayInTheFuture to 30
	dayInTheFuture = 30;

	// Copy the value from dayInTheFuture to 
	// dayOfTheMonth
	dayOfTheMonth = dayInTheFuture;

	// Declaration of a constant integer
	// Use all uppercase characters for constant names.
	// Two reasons that we declare constants
	//  1) The value may need to change as the application domain changes.
	//  2) Make the code easier to read.
	const int DAYS_PER_WEEK = 7;
	const int MAX_CAR_IN_LC_PARKINGLOT = 1000;
	
	// Operations on integers

	// Add one to the day of the Month
	dayOfTheMonth = dayOfTheMonth + 1;

	// Subtract one from the day of the Month
	dayOfTheMonth = dayOfTheMonth - 1;

	// Divide the dayOfTheMonth by 2
	// The result here will be 15
	
	// 30 / 2 has remainder 0

	cout << "Enter a value < 64: ";
	cin >> dayOfTheMonth;

	int remainder = dayOfTheMonth % 2;
	cout << remainder << endl;
	dayOfTheMonth = dayOfTheMonth / 2;

    remainder = dayOfTheMonth % 2;
	cout << remainder << endl;
	dayOfTheMonth = dayOfTheMonth / 2;

	remainder = dayOfTheMonth % 2;
	cout << remainder << endl;
	dayOfTheMonth = dayOfTheMonth / 2;

	remainder = dayOfTheMonth % 2;
	cout << remainder << endl;
	dayOfTheMonth = dayOfTheMonth / 2;

	remainder = dayOfTheMonth % 2;
	cout << remainder << endl;
	dayOfTheMonth = dayOfTheMonth / 2;

	remainder = dayOfTheMonth % 2;
	cout << remainder << endl;
	dayOfTheMonth = dayOfTheMonth / 2;

	// The result here will be 7 because
	// integer division discards any fractional
	// component.

	// 7 / 2 has remainder 1
	remainder = dayOfTheMonth % 2;
	dayOfTheMonth = dayOfTheMonth / 2;
	cout << "The value of dayOfTheMonth is " <<
		dayOfTheMonth << endl;
	// Other operations include
	//  * for multiplication
	//  / for integer division
	//  % for modulo (remainder)

	// Add one to an integer
	int dayOfTheYear = 24;

	// Same as dayOfTheYear = dayOfTheYear + 1;
	++dayOfTheYear;  

	// 32-bit floating point value
	float pi = 3.14f;

	// double
	// 64-bit floating point value
	double piAsDouble = 3.14;

	cout << "The value of the pi variable is " << pi << endl;
	cout << "The value of the piAsDouble variable is " 
		<< pi << endl;

	// sizeof returns the number of bytes used
	// to represent a variable or data type
	cout << "The size of double in bytes is " <<
		sizeof(double) << endl;

	cout << "The size of double in bytes is" <<
		sizeof(piAsDouble) << endl;

	// The bool data type has only two possible 
	// values (true and false)
	bool todayIsFriday = true;
	bool todayIsThursday = false;
	todayIsFriday = false;

	// bool
	cout << "Size of bool is " << 
		sizeof(bool) << " byte." << endl;

	// mixed data types
	int xPi = (int) 3.14f;    // 

	int i = 10;
	float a = (float) i;
	int b = 2;

	int x = 1;
	int y = 2;
	float z = (float) (x / y); // z will be zero
	z = ((float)x) / ((float)y);  // z will be .5f

	char anA = 'A';  // 'A' is 65 in ascii
	cout << anA << endl; // prints A
	cout << (int)anA << endl; // prints 65
	
	// Explicit data type conversion in c++ are
	// called "casts."
	// The oldest cast is c++ just uses the destination
	// data type name in parentheses to indicate the
	// data type conversion.

	// Casts do not change the value in the variable
	// at all.


	float c = 1.0f / 2.0f;


	float circ = (float) (2 * x * 3);

	// Mixed-mode (mixed data type operations)
	float fraction = (float) (1.0 / (double) 2);
	
	// int someInt = (int) (((float) 3) * fraction);
	int someInt = 3 * fraction;

	char someChar = (char)66;
	cout << someChar << endl; // prints B
	int anotherInt = someChar; // (int) someChar
	cout << anotherInt << endl; // print 66
	double someDouble = 'B';  // (double) 'B'

	// Prefer explicit casts to implicit casts
	cout << "Buy these item:\n" 
		<< "\tEggs\n"
		<< "\tMilk" << endl;


	system("pause");
}