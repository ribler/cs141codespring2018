#include <iostream>
using namespace std;


int main()
{
	// Get an integer larger than 0 from the user.
	// If the user provides a bad input, let the user
	// know and ask again until you get a good input.


	int userInput;
	do
	{
		cout << "Enter a number greater than zero: ";
		cin >> userInput;
		if (userInput <= 0)
		{
			cout << userInput << " is not greater than zero." << endl;
			cout << "Please try again." << endl;
		}
	} while (userInput <= 0);

	// Sum all the integers from 1 to the number the user
	// entered.
	int sum = 0;
	for (int counter = 1; counter <= userInput; ++counter)
	{
		sum = sum + counter;
	}
	cout << "The sum of all integers from 1 to " << userInput
		<< " is " << sum << endl;

	// Double the users number until it exceeds some target number.
	// Report the number of times that you had to double the
	// number.
	const int TARGET_NUMBER = 1000000;
	int doubleCounter = 0;
	int numberToDouble = userInput;
	while (numberToDouble <= TARGET_NUMBER)
	{
		numberToDouble = numberToDouble * 2;
		++doubleCounter;
	}
	cout << "The number " << userInput << " was doubled "
		<< doubleCounter << " times to exceed " << TARGET_NUMBER
		<< endl;

	// Find all the integer less than the number that divide
	// the number evenly. Print each one on its own line.
	for (int counter = 1; counter < userInput; ++counter)
	{
		int remainder = userInput % counter;
		if (remainder == 0)
		{
			cout << counter << " divides evenly into " << userInput
				<< endl;
		}
	}




	system("pause");
}