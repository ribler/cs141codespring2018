#include <iostream>
using namespace std;

// Write a function that counts the words in a string
int countWords(const char* sentence)
{
	int wordCount = 0;
	if (*sentence != 0)
	{
		while (*sentence != 0)
		{
			if (*sentence == ' ')++wordCount;
			++sentence;
		}
		++wordCount;
	}
	return wordCount;
}

char* allocateAndCopyWord(const char* startPtr,
	const char* endPtr)
{
	// Compute the length of the word.
	int length = endPtr - startPtr + 1;

	char* word = new char[length];

	int i = 0;
	while (startPtr != endPtr)
	{
		word[i++] = *startPtr++;
	}
	word[i] = 0;

	return word;
}

// Split a string into words
// Return a dynamically allocated array of pointers to 
// the dynamically allocated copies of the words.
//char** wordArray = splitToWords("Test this sentence");
//
//cout << wordArray[0] << endl;  // prints "Test"
//cout << wordArray[1] << endl;  // prints "this"
//cout << wordArray[2] << endl;  // prints "sentence"
char** splitToWords(const char* sentence)
{
	int nWords = countWords(sentence);

	// Allocate an array of char*, one char* for each word
	char** theWords = new char*[nWords+1];

	const char* startOfWord = sentence;
	const char* endOfWord = startOfWord;
	int nextWordIndex = 0;

	while (*endOfWord != 0)
	{
		// Find the end of the next word
		while (*endOfWord != ' ' && *endOfWord != 0)
		{
			++endOfWord;
		}

		theWords[nextWordIndex] =
			allocateAndCopyWord(startOfWord, endOfWord);

		++nextWordIndex;

		// Move to the next word.k
		if (endOfWord != 0)
		{
			startOfWord = endOfWord + 1;
			endOfWord = startOfWord;
		}
	}

	theWords[nextWordIndex] = 0;

	return theWords;
}





void swapPairs(char* theString)
{
	char* secondPtr = theString + 1;
	while (*theString != 0 && *secondPtr != 0)
	{
		char temp = *theString;
		*theString = *secondPtr;
		*secondPtr = temp;
		secondPtr++;
		secondPtr++;
		theString++;
		theString++;
	}
}

// Write a function that reverses an array of doubles
void reverse(double* thingToReverse, int nElements)
{
	double* frontPtr = thingToReverse;
	double* endPtr = thingToReverse + nElements - 1;
	while (frontPtr < endPtr)
	{
		// Exchange the values that frontPtr and
		// endPtr are pointing to.

		// Save the value that frontPtr points to
		// before we clobber it.
		double temp = *frontPtr;
		*frontPtr = *endPtr;
		*endPtr = temp;

		++frontPtr;
		--endPtr;
	}
}

int stringLength(const char* theString)
{
	int length = 0;
	while (*theString != 0)
	{
		++length;
		++theString;
	}
	return length;
}

// Allocate and copy a null-terminated char string
char* allocateAndCopy(const char* theString)
{
	// dynamically allocate enough memory to hold the copy
	int length = stringLength(theString);
	char* copy = new char[length+1];
	char* startOfCopy = copy;

	// Using pointer notation
	for (int i = 0; i <= length; i++)
	{
		*copy++ = *theString++;
	}
	// *copy = 0;

	return startOfCopy;
}

// Allocate and copy a null-terminated char string
char* allocateAndCopy2(const char* theString)
{
	// dynamically allocate enough memory to hold the copy
	int length = stringLength(theString);
	char* copy = new char[length + 1];

	// Using array notation
	for (int i = 0; i <= length; i++)
	{
		copy[i] = theString[i];
	}

	return copy;
}

// Write a function that changes negative values to their
// squares.
void  squareNegatives(double* array,
	int nElements)
{
	//for (int i = 0; i < nElements; i++)
	//{
	//	if (array[i] < 0) array[i] = array[i] * array[i];
	//}

	// Here is how to write it using the dereference operator.
	for (int i = 0; i < nElements; i++)
	{
		if (*array < 0) *array = *array * *array;
		++array;
	}
}

// Write a function that prints the values in a double array
void  printArray(const double* array, int nElements)
{
	const double* startOfArray = array;
	// Use the dereference operator to write out the 
	// value of each element in the array.
	for (int i = 0; i < nElements; i++)
	{
		// Now that it is const double* we can't change it.
		// *array = 2 * *array;
		cout << *array++ << endl;
	}

	// Use array notation to write the value of each 
	// element in the array.
	for (int i = 0; i < nElements; i++)
	{
		cout << startOfArray[i] << endl;
	}

}


int main()
{

	char** wordArray = splitToWords("Test this sentence");
	
	int i = 0;
	while (wordArray[i] != 0)
	{
		cout << wordArray[i] << endl;
		++i;
	}

	//cout << wordArray[0] << endl;  // prints "Test"
	//cout << wordArray[1] << endl;  // prints "this"
	//cout << wordArray[2] << endl;  // prints "sentence"

	 double someArray[] = { 2.0, 3.0, 4.0 };
	printArray(someArray, 3);


	char* myCopy = allocateAndCopy("abcdefg");

	// prints "abcdefg"
	cout << myCopy << endl;
	delete[] myCopy;
	// Determine the day of the week given
	// the month and day (for 2018 only)

	// Prompt the user for the month
	//  -- (integer 1-12)
	// Read the integer from the keyboard
	int monthNumber;
	cout << "Please enter the month number (1-12):";
	cin >> monthNumber;


	const int N_STUDENTS = 100;
	const int ASSIGNMENTS = 10;

	const int N_CLASS = 2;

	float scores[N_CLASS][N_STUDENTS][ASSIGNMENTS];

	// Set all the values in the array to 100

	// For every class, starting with class 0 
	for (int classNumber = 0; classNumber < N_CLASS; ++classNumber)
	{
	    // For every row, starting with row 0
		for (int studentNumber = 0; studentNumber < N_STUDENTS; ++studentNumber)
		{
			// For every column, starting with column 0
			for (int assignment = 0; assignment < ASSIGNMENTS
				; ++assignment)
			{
				scores[classNumber][studentNumber][assignment] = 0.0f;
			}
		}
	}

	// Dynamically allocate an array of 10 floats
	//   
	// new k return a k*

	float anotherArray[10];

	float* myArray = new float[10];
	for (int i = 0; i < 10; i++)
	{
		myArray[i] = (float) i;
	}

	delete[] myArray;

	// Allocate a 2d array 

	int nRows = 10;
	int nColumns = 11;

	// Allocate an array of float pointers, one for
	// each row.
	float** my2DArray = new float*[nRows];

	// Allocate the memory to hold the data for each row
	for (int row = 0; row < nRows; ++row)
	{
		my2DArray[row] = new float[nColumns];
	}

	for (int row = 0; row < nRows; ++row)
	{
		for (int column = 0; column < nColumns; ++column)
		{
			my2DArray[row][column] = 0.0f;
		}
	}

	// Delete the 2d array

	// First delete all the row data

	for (int row = 0; row < nRows; ++row)
	{
		delete[] my2DArray[row];
	}

	delete[] my2DArray;






	// See if the year is a leap year
	// It is a leap year if it is 
	// divisible by 400 or if it is divisible by 4 and not 100
	int year = -1;
	//cout << "Enter year: ";
	// cin >> year;

	while (year < 1600 || year > 2100)
	{
		cout << "Enter year: ";
		cin >> year;
	}

	do
	{
		cout << "Enter year: ";
		cin >> year;
	} while (year < 1600 || year > 2100);


	if (((year % 400) == 0) ||
		((year % 4) == 0) && (year % 100 != 0))
	{
	}

	while (!(year >= 1600 && year <= 2100))
	{
	}

	while (year < 1600 || year > 2100)
	{

	}






	// Prompt the user for the day
	// -- (integer 1-31)
	// Read the integer from the keyboard

	// Lookup the offset for the month
	//  -- use a switch statement
	// Jan=0, Feb=3, Mar=3, Apr=6, May=1,
	// Jun=4, Jul=6, Aug=2, Sep=5, Oct=0,
	// Nov=3, Dec=5

	// Compute the integer day as
	//  (monthOffset + day) % 7

	// Print the day of the week
	//  -- use a switch statement 

	system("pause");
}