#include <iostream>
using namespace std;


void findTwoLargest(const float* theArray, int length,
	float& largest, float& secondLargest)
{
	largest = theArray[0];
	for (int i = 1; i < length; i++)
	{
		// Is the ith element larger than the largest I've seen so far
		if (largest < theArray[i])
		{
			largest = theArray[i];
		}
	}

	secondLargest = theArray[0];
	for (int i = 1; i < length; i++)
	{
		// Is the ith element larger than the largest I've seen so far
		if ((secondLargest < theArray[i] && 
			theArray[i] != largest) || secondLargest == largest)
		{
			secondLargest = theArray[i];
		}
	}
}

float** allocate2D(int nRows, int nColumns)
{
	float** theRowPtrs = new float*[nRows];

	for (int i = 0; i < nRows; i++)
	{
		theRowPtrs[i] = new float[nColumns];
	}

	for (int row = 0; row < nRows; ++row)
	{
		float rowValue = row % 2;
		for (int column = 0; column < nColumns; ++column)
		{
			theRowPtrs[row][column] = rowValue;
		}
	}
	return theRowPtrs;
}

void deallocate3DArray(float*** thingToDelete, int nTables,
	int nRows)
{
	for (int table = 0; table < nTables; ++table)
	{
		for (int row = 0; row < nRows; ++row)
		{
			delete[] thingToDelete[table][row];
		}

		delete[] thingToDelete[table];
	}

	delete[] thingToDelete;
}


float sumFloats(const float* theArray, int nElements)
{
	float sum = 0;

	// For every element in the array
	for (int i = 0; i < nElements; i++)
	{
		sum += theArray[i];
	}

	// Alternative implementation using pointers
	//for (int i = 0; i < nElements; i++)
	//{
	//	sum += *theArray;
	//	++theArray;
	//}

	return sum;
}

// This function is unfinished.
void changeAfterHyphen(char* theString,
	char subChar)
{
	// Print a char, but I have a char*
	//cout << *theString << endl;  // print the first char in array
	//cout << theString[0] << endl; // print the first char in array

	//cout << *(theString + 1) << endl; // second char
	//cout << theString[1] << endl;  // second char



	char* subPtr = theString;



	const int SUB_OFFSET = 2;

	// While we haven't counted to 2 and we haven't
	// encountered a null-terminator increment the subPtr
	for (int i = 0; i < SUB_OFFSET && *subPtr != 0; i++)
	{
		++subPtr;
	}

	while (*subPtr != 0)
	{
		if (*theString == '-')
		{
			*subPtr = subChar;
		}
		++subPtr;
		++theString;
	}
}

int main()
{
	// Question 2
	const int N_FLOATS = 4;
	const float myFloats[] = { 3.0f, 5.0f, 13.0f, 2.0f };
	float sum = sumFloats(myFloats, N_FLOATS);
	cout << sum << endl;   // should print 23

	// Question 3
	char myArray[] = "a-bc-defg-h";
	changeAfterHyphen(myArray, 'x');
	cout << myArray << endl;    // prints a-bx-dxg-h 

	myArray[2] = 0;
	changeAfterHyphen(myArray, 'x');
	cout << myArray << endl;    // prints a-

	// Question 4
	float largest;
	float secondLargest;
	findTwoLargest(myFloats, N_FLOATS, largest, secondLargest);
	cout << "largest is " << largest << endl;  // prints 13
	cout << "second largest is " << secondLargest << endl;  // prints 5

	//// I have not provided tests for questions 5 and 6.

	//// Question 7
	//const int N_DOUBLES = 13;
	//double myDoubles[N_DOUBLES] = { 2, 1, 4, 1, 8, 2, 2, 5, 2.5, 2, 7 , 4, 6 };
	//partitionOnThree(myDoubles, N_DOUBLES);

	//// prints 2, 1, 2, 1, 2.5, 2, 2, 5, 8, 4, 7, 4, 6,
	//for (int i = 0; i < N_DOUBLES; i++)
	//{
	//	cout << myDoubles[i] << ", ";
	//}
	//cout << endl;

	system("pause");
}