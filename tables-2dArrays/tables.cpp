#include <iostream>
using namespace std;

int main()
{
	const int MAX_STUDENTS = 3;
	const int MAX_TESTS = 4;

	// Create a table to hold all the test scores
	// When we create the table we first specify 
	// the number of row then columns
	//  float myArray[N_ROWS][N_COLUMNS]
	float scores[MAX_STUDENTS][MAX_TESTS];

	// For every row i
	//   For every column j
	//      scoures[i][j] = 100.0f;
	scores[0][0] = 100.0f;
	scores[0][1] = 100.0f;
	scores[0][2] = 100.0f;
	scores[0][3] = 100.0f;
	scores[1][0] = 100.0f;
	scores[1][1] = 100.0f;
	scores[1][2] = 100.0f;
	scores[1][3] = 100.0f;
	scores[2][0] = 100.0f;
	scores[2][1] = 100.0f;
	scores[2][2] = 100.0f;
	scores[2][3] = 100.0f;

	// Same as the instructions listed above.
	for (int row = 0; row < MAX_STUDENTS; ++row)
	{
		for (int column = 0; column < MAX_TESTS; ++column)
		{
			scores[row][column] = 100.0f;
		}
	}

	// Input the scores for all students for all 
	// tests.
	for (int student = 0; student < MAX_STUDENTS; ++student)
	{
		for (int test = 0; test < MAX_TESTS; ++test)
		{
			cout << "Enter score for student "
				<< student + 1 << " test "
				<< test + 1 << ": ";
			cin >> scores[student][test];
		}
	}

	// Input the scores for all students for all 
	// tests.	Asking for all the test 1 scores first.
	for (int test = 0; test < MAX_TESTS; ++test)
	{
		for (int student = 0; student < MAX_STUDENTS; ++student)
		{
			cout << "Enter score for student "
				<< student + 1 << " test "
				<< test + 1 << ": ";
			cin >> scores[student][test];
		}
	}

	// Find the highest of all the scores
	// Set the highest so far to scores[0][0]
	// Look all the other scores
	// When we see something higher replace
	// highest so far

	float highestSoFar = scores[0][0];
	for (int test = 0; test < MAX_TESTS; ++test)
	{
		for (int student = 0; student < MAX_STUDENTS; ++student)
		{
			if (highestSoFar < scores[student][test])
			{
				highestSoFar =
					scores[student][test];
			}
		}
	}
	cout << "Highest score is " 
		<< highestSoFar << endl;

	// Find the highest score on the first test.
	highestSoFar = scores[0][0];
	for (int student = 0; student < MAX_STUDENTS; ++student)
	{
		if (scores[student][0] > highestSoFar)
		{
			highestSoFar = scores[student][0];
		}
	}
	cout << "highest is " << highestSoFar << endl;

	system("pause");
}