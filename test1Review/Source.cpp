#include <iostream>
using namespace std;

int main()
{
	const int N_STUDENTS = 128;
	float studentScores[N_STUDENTS];
	const float LOWEST_A = 90.0f;
	const float LOWEST_B = 77.0f;
	const float LOWEST_C = 68.0f;
	const float LOWEST_D = 55.0f;
	const float LOWER_THAN_POSSIBLE = -1;

	const int N_DIFFERENT_GRADES = 5;

	float lowestGrade[N_DIFFERENT_GRADES];
	lowestGrade[0] = LOWEST_A;
	lowestGrade[1] = LOWEST_B;
	lowestGrade[2] = LOWEST_C;
	lowestGrade[3] = LOWEST_D;
	lowestGrade[4] = LOWER_THAN_POSSIBLE;

	int nOFEachGrade[N_DIFFERENT_GRADES];

	// Set all counts to zero
	for (int i = 0; i < N_DIFFERENT_GRADES; i++)
	{
		nOFEachGrade[i] = 0;
	}

	// For each grade
	for (int i = 0; i < N_STUDENTS; i++)
	{
		int gradeIndex = 0;
		while (studentScores[i] < lowestGrade[gradeIndex])
		{
			// Test the next lowest grade.
			++gradeIndex;
		}

		// Add one to the counter for that grade.
		++nOFEachGrade[gradeIndex];
	}

	for (int i = 0; i < N_DIFFERENT_GRADES-1; i++)
	{
		cout << (char)('A' + i) << ": " << nOFEachGrade[i] 
			<< endl;
	}

	cout << "F: " << nOFEachGrade[N_DIFFERENT_GRADES - 1] << endl;

	system("pause");
}