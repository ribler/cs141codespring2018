#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
	// Create an array of length ARRAY_SIZE
	// Set the elements in the array to the integers
	// 1 through ARRAY_SIZE;

	int x = 10;

	// Print the integer so that it takes up 4 columns

	cout << setw(4) << x << endl;
	
	const int ARRAY_SIZE = 3;
	int myArray[ARRAY_SIZE];

	for(int i=0;i<ARRAY_SIZE;i++)
	{ 
		myArray[i] = i + 1;
	}

	// Create a floating point array of length ARRAY_SIZE
	// set each value in the array to START_VALUE.
	const float START_VALUE = 1000000.0f;

	// Create an floating-point array of length ARRAY_SIZE
	float newArray[ARRAY_SIZE];
	for (int i = 0; i < ARRAY_SIZE; i++) 
	{
		newArray[i] = START_VALUE;
	}

	// Create a floating point array of length ARRAY_SIZE
	// Set the first value to START_VALUE
	// Set subsequent values to the previous array value / 2.0f
	//   Think about setting the first element of the array 
	//   outside of the loop.
	float halfArray[ARRAY_SIZE];

	// Set the value of the first element of the array to START_VALUE
	halfArray[0] = START_VALUE;

	// Set the remaining items in the array
	for (int i = 1; i < ARRAY_SIZE; i++)
	{
		halfArray[i] = halfArray[i - 1] / 2.0f;
	}


	// Set the elements in an integer array of size ARRAY_SIZE
	// to their index number
	int lastArray[ARRAY_SIZE];
	for (int i = 0; i < ARRAY_SIZE; i++)
	{
		lastArray[i] = i;
	}




	
}