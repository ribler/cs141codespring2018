#include <iostream>
using namespace std;

int main()
{
	// Programming Style 
	//  - Developed to make programs easier to read.
	//  - Industry-wide conventions
	//     -- c++ constant names should be all caps
	//     -- c++ variables start with lowercase letters
	//     -- Use numeric literals only for const definitions
	//        -- With a few exceptions (1, 0, -1) or numbers that represent themselves


	// Legal, but violations of style standards
	// int X = 23;
	// int MyVariable = 10;
	// const int max_int = 102;

	//float purchasePriceInDollars;
	//cout << "Enter the purchase price: ";
	//cin >> purchasePriceInDollars;    // Someone enter 1.25

	//int purchasePriceInCents = (int) (purchasePriceInDollars * 100);


	int myVariable;
	//
	//....
	//
	myVariable = 10;
	//
	//....
	//
	const int MIN_SOMETHING = 10;
	if (MIN_SOMETHING == myVariable)
	{

	}

	if (myVariable == 0)
	{
	}


	const int MAX_INT = 1002;
	const int MIN_INT = -23;
	cout << "Please enter an integer in the range [" <<
		MIN_INT << ", " << MAX_INT << "]: ";

	// A variable my be referenced from its point of declaration
	// to the enclosing curly brace }.
	int enteredInt;
	cin >> enteredInt;

	cout << "You entered: " << enteredInt << endl;

	// In C++, anywhere we can put a simple statement,
	// we can put a "compound statement," where 
	// "compound statement is defined by a set of 
	// curly braces {}.

	// The if-statement allows us to specify code that
	// gets executed under particular conditions.

	// I want to complain if the user didn't follow the 
	// directions.
	// "if" is a c++ keyword
	// syntax for if-statement is:
	// if ( <some boolean expression> )
	// {
	//    code that gets executed if the boolean expression is true
	// }
	// -- optionally we can add an else clause
	// else
	// {
	//    code that gets executed if the boolean expression is false
	// }
	// C++ we have the following boolean operators:
	//   > Greater than
	//   < Less than
	//   >= Greater than or equal to
	//   <= Less than or equal to
	//   == Equal to
	//   != Not Equal to
	//   ! Not
	//
	// Boolean expressions can be combined using the boolean operators
	//   &&  for AND (true if both conditions are true)
	//   ||  for OR  (true if one or both conditions are true


	// Two reasons to use name constants
	// 1) Easier to read/understand the code
	// 2) Easier to change
	//      -- Change in one place and everything works.
	//if (enteredInt > 102 || enteredInt < -23)
	//{

	//}

	// Check to see if the entered value is > MAX_INT
	if (enteredInt > MAX_INT || enteredInt < MIN_INT)
	{
		// If this statement is reached, then the user entered
		// the integer incorrectly.

		// The code inside the brackets is executed
		// only if the condition in the if-statement is true.
		cout << enteredInt << " is not in the range [" <<
			MIN_INT << ", " << MAX_INT << "]" << endl;
		cout << "Please read the instructions more carefully in the future."
			<< endl;

		const int REALLY_BIG = MAX_INT * 100;
		if (enteredInt > REALLY_BIG)
		{
			cout << "Wow, that is really a lot bigger than "
				<< MAX_INT << endl;
		}

		// Use ctrl-k ctrl-d to reindent the code to make it more
		// readable.

		if (enteredInt < 0)
		{
			int shortTimeVariable = 10;
			cout << enteredInt << " is less than 0." << endl;
			cout << "shortTimeVariable is " << shortTimeVariable << endl;
		}
		// shortTimeVaraible can't be referenced here.
		//cout << "shortTimeVariable is " << shortTimeVariable << endl;
	}
	else
	{
		// If this statement is reached, the user entered the integer
		// correctly
		cout << "Wow, you are really good at following instructions!"
			<< endl;
	}

	if (enteredInt > 0)
	{
		cout << "enteredInt is greater than zero!" << endl;
	}

	// Determine the number days in a month
	//  - not worrying about leap years yet.

	cout << "Enter a month number [1=Jan, 7=Jul]: ";
	int monthNumber;
	cin >> monthNumber;

	int numberOfDaysInMonth;
	// switch syntax:
	// switch (<integer value>)
	// {
	//    case <integer constant>:
	//     
	// }

	const int JAN = 1;
	const int FEB = 2;
	const int MAR = 3;
	const int APR = 4;
	const int MAY = 5;
	const int JUN = 6;
	const int JUL = 7;
	const int AUG = 8;
	const int SEP = 9;
	const int OCT = 10;
	const int NOV = 11;
	const int DEC = 12;

	switch (monthNumber)
	{
	case JAN:
	case MAY:
	case MAR:
	case JUL:
	case AUG:
	case OCT:
	case DEC:
		// set the number days for January (31)
		numberOfDaysInMonth = 31;

		// Indicate the end of the case with a break-statement.
		break;

		// Feburary
	case FEB:
		// The year is a leap year if it is divisible by
		// 400 or divisibly by 4 and not by 100
		int year;
		cout << "Enter the year: ";
		cin >> year;
		// Write an if-statement to determine if this is
		// a leap year
		// Homework assignment - write the if-statement 
		// to determine if the year is a leap year.
		if (((year % 4 == 0) && (year % 100 != 0))
			|| year % 400 == 0)
		{
			numberOfDaysInMonth = 29;
		}
		else
		{
			numberOfDaysInMonth = 28;
		}
		break;

		// March
	case APR:
	case JUN:
	case SEP:
	case NOV:
		numberOfDaysInMonth = 30;
		break;
		// This case gets executed if none of the 
		// other cases match.
	default:
		cout << "No idea.  Later we will know." << endl;
		numberOfDaysInMonth = 0;
		break;
	}

	if (numberOfDaysInMonth > 0)
	{
		cout << "That month has " << numberOfDaysInMonth
			<< " days in it." << endl;
	}

	// Determine if you can drive.

	// Prompt the user and get values for the
	// ages of the driver and the passenger.

	int driversAge;
	int passengersAge;

	cout << "Enter the driver's age: ";
	cin >> driversAge;

	cout << "Enter the passenger's age: ";
	cin >> passengersAge;

	// Anyone over 16 can drive alone at any time
	const int MIN_AGE_TO_DRIVE_ALONE = 17;

	// Try to make boolean variables ask a true/false
	// question or specify a true/false condition
	// e.g., isLegalDriver, ageIsAboveLimit, etc.
	// 
	bool driverCanDrive =
		driversAge >= MIN_AGE_TO_DRIVE_ALONE;

	if (driversAge >= MIN_AGE_TO_DRIVE_ALONE)
	{
		cout << "Driver may drive legally." << endl;
	}

	// Drivers may drive if they are 15 or older 
	// and the passenger is 21 or older
	const int MIN_AGE_TO_DRIVE_WITH_ADULT = 15;
	const int MIN_AGE_TO_DRIVE_WITH_LEARNER = 21;

	if (driversAge >= MIN_AGE_TO_DRIVE_WITH_ADULT &&
		passengersAge >= MIN_AGE_TO_DRIVE_WITH_LEARNER)
	{
		cout << "Driver may drive legally." << endl;
	}

	// Combine the previous two if-statements into
	// one statement.
	if ((driversAge >= MIN_AGE_TO_DRIVE_ALONE) ||
		(driversAge >= MIN_AGE_TO_DRIVE_WITH_ADULT &&
			passengersAge >= MIN_AGE_TO_DRIVE_WITH_LEARNER))
	{
		cout << "Driver may drive legally." << endl;
	}

	bool canDrive = ((driversAge >= MIN_AGE_TO_DRIVE_ALONE) ||
		(driversAge >= MIN_AGE_TO_DRIVE_WITH_ADULT &&
			passengersAge >= MIN_AGE_TO_DRIVE_WITH_LEARNER));

	if (canDrive)
	{
		cout << "Driver may drive legally." << endl;
	}

	// 
	if (true)
	{
		cout << "always do this!" << endl;
	}
	else
	{
		cout << "never do this!" << endl;
	}


	system("pause");
}