#include <iostream>
using namespace std;

int main()
{

	// Often we want to execute the same code
	// more than once.

	// Write Hello world! over and over again
	// while ( <condition> ) compound statement

	const int NUMBER_OF_TIMES_TO_SAY_HELLO = 10;
	int numberOfTimesWeSaidHello = 0;

	while (numberOfTimesWeSaidHello <
		NUMBER_OF_TIMES_TO_SAY_HELLO)
	{
		cout << "Hello world!" << endl;
		numberOfTimesWeSaidHello = 
			numberOfTimesWeSaidHello + 1;
	}

	// Sum the integers from 1 to LAST_INT_IN_SUM
	const int LAST_INT_IN_SUM = 10;
	int sum = 0;
	int nextIntInSum = 1;

	while (nextIntInSum <= LAST_INT_IN_SUM)
	{
		// Add the integer to the sum
		sum = sum + nextIntInSum;

		// Determine the next int to add
		nextIntInSum = nextIntInSum + 1;
	}
	
	cout << "The sum of integers 1 to " <<
		LAST_INT_IN_SUM << " is " << sum << endl;

	// How many integers must we sum to get to 
	// a value greater than a TARGET_SUM?
	const int TARGET_SUM = 1000000;

	sum = 0;
	nextIntInSum = 1;
	while (sum <= TARGET_SUM)
	{
		sum = sum + nextIntInSum;

		// Compute the next integer only if
		// we haven't reached the target.
		if (sum <= TARGET_SUM)
		{
			nextIntInSum = nextIntInSum + 1;
		}
	}

	cout <<
		"The number of integers that must be summed to exceed "
		<< TARGET_SUM << " is " << nextIntInSum << endl;
	cout << "That sum is " << sum << endl;

	// The do-while statement checks the loop condition at the
	// end of the loop.  We always execute the body of a do-while
	// statement at least once.
	int userInput;
	do
	{
		cout << "Enter an integer greater than 0: ";
		cin >> userInput;

		if (userInput <= 0)
		{
			cout << userInput << " is not greater than 0.";
		}
	} while (userInput <= 0);

	cout << "You entered " << userInput << "!" << endl;

	// For-loops
	//   Use for-loops when you can calculate the number of times
	// that you want to execute the loop.
	// syntax: for ( <a statement that gets executed when we enter
	// the loop> ; <loop-condition> / <a statement that gets executed
	// when we reach the end of the body of the loop> )\
	// { <body of the loop}

	// To execute a loop n times do this:
	int n = 3;
	for (int helloCounter = 0; helloCounter < n;
		helloCounter = helloCounter + 1)
	{
		cout << "Hello world!" << endl;
	}

	// To do something n times do this:
	for (int i = 0; i < n; ++i)
	{
		// This will be done n times.
	}

	// Write integers from 2 to 200 that are even
	const int START_EVEN = 2;
	const int END_EVEN = 200;
	for (int i = START_EVEN; i <= END_EVEN; i = i + 2)
	{
		cout << i << endl;
	}

	// Count backward from 100
	const int START_BACKWARD = 100;
	for (int i = START_BACKWARD; i >= 0; i = i - 1)
	{
		cout << i << endl;
	}

	// This while loop will do the same thing.
	int j = START_BACKWARD;
	while (j >= 0)
	{
		cout << j << endl;
		j = j - 1;
	}
	
	// When to use which loop
	//  If you can easily calculate the number of times the loop
	//  will execute, use a for-loop.

	//  If you cannot easily calculte the number of times the loop
	//  will execute and you might execute the loop zero times,
	//  use a while-loop

	//  If you cannot easily calculte the number of times the loop
	//  will execute and you always want to execute the loop at least
	//  once, use a do-while-loop.

	// Get the degree of an angle between 0 and 360 degrees.
	float degrees;
	const float MIN_DEGREES = 0;
	const float MAX_DEGREES = 360;
	
	do
	{
		// Prompt the user for the degrees
		cout << "Please enter degrees in the range ["
			<< MIN_DEGREES << ", " << MAX_DEGREES << "]: ";
		cin >> degrees;
		
		// Write an error message if degrees is outside the range
		if (degrees < MIN_DEGREES || degrees > MAX_DEGREES)
		{
			cout << degrees << " is not in the range ["
				<< MIN_DEGREES << ", " << MAX_DEGREES << "]" << endl;
			cout << "Please try again." << endl;
		}

		// Check the loop condition at the bottom of the loop.

	} while (degrees < MIN_DEGREES || degrees > MAX_DEGREES);

	// Incrementing integers

	// preincrement
	int y = 10;

	// increment y with preincrement
	// This adds one to y (y will change to 11)
	// Similar to y=y+1;
	// The pre in preincrement means increment the value before
	// the expression is evaluated;
	++y;

	int z = ++y;    // z will be set to 12

	// increment y with postincrement
	// The post in postincrement means increment the value
	// after the expression is evaluated.

	z = y++;      // z will still be set to 12 even though y is now 13.

	cout << y++ << endl;  // What does this print? 13

	cout << ++y << endl;   // What does this print? 15

	// First there was the c language
	int c = 0;  

	// Then C++
	cout << c++ << endl;  // prints 0
	cout << c << endl;    // prints 1

	// Other shortcuts for assignment
	//c = c + MIN_DEGREES * MAX_DEGREES;
	//c += (MIN_DEGREES * MAX_DEGREES);

	// c = c * MAX_DEGREES;
	// c *= MAX_DEGREES;


	system("pause");
}