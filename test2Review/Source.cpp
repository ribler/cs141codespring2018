#include <iostream>
using namespace std;

// Allocate for a 2d array
float** allocate2D(int nRows, int nColumns)
{
	// Allocate an array of float*, one for each row
	float** rows = new float*[nRows];

	// Allocate the data for each row
	for (int i = 0; i < nRows; i++)
	{
		rows[i] = new float[nColumns];
	}

	return rows;
}

void delete2DArray(float** thingToDelete,
	int nRows)
{
	// Delete the data in the rows
	for (int i = 0; i < nRows; i++)
	{
		delete [] thingToDelete[i];
	}

	// Delete the array of pointers to row data.
	delete[] thingToDelete;
}

int stringLength(const char* theString)
{
	int length = 0;
	while (*theString != 0)
	{
		++theString;
		++length;
	}
	return length;
}
// The function takes const char* parameters so that constant strings
// can be used as inputs (in addition to non-constant strings).
// It returns a pointer to a dynamically allocated character string
// that contains the result.
char* interleave(const char* firstString, const char* secondString)
{
	// Determine the length of the interleaved string
	//    It will be the sum of the lengths of the two strings + 1 for the
	//    null-terminator.
	int length = stringLength(firstString) + 
		stringLength(secondString) + 1;

	// Allocate memory to hold the interleaved string
	char* newString = new char[length];

	// We need to remember the first address of new string
	// so that we can return it.  
	char* startOfNewString = newString;

	// While you still have chars in both strings
	// copy the interleaved values
	while (*firstString != 0 && *secondString != 0)
	{
		// Copy a char from each string, then increment the pointers.
		*newString++ = *firstString++;
		*newString++ = *secondString++;
	}

	// If one of the input strings is longer than the
	// other, we will need to copy the excess to newString.
	// Set largerString to point to the point where we
	// stopped interleaving the larger string.
	const char* largerString;
	if (*firstString != 0)
	{
		largerString = firstString;
	}
	else
	{
		largerString = secondString;
	}

	// Copy the excess to newString if there is any.
	while (*largerString != 0)
	{
		*newString++ = *largerString++;
	}

	// Null-terminate the new string
	*newString = 0;

	return startOfNewString;
}


int main()
{
	const int N_ROWS = 4;
	const int N_COLUMNS = 6;
	float** my2DArray = 
		allocate2D(N_ROWS, N_COLUMNS);

	// Set all the values in my2DArray to zero
	for (int i = 0; i < N_ROWS; i++)
	{
		for (int j = 0; j < N_COLUMNS; j++)
		{
			my2DArray[i][j] = 0.0f;
		}
	}

	// Delete the 2D array

	// Delete the row data
	for (int i = 0; i < N_ROWS; i++)
	{
		delete[] my2DArray[i];
	}

	// Delete the pointers to the row data
	delete[] my2DArray;


	system("pause");
}